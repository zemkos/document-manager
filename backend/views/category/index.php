<?php

use common\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'name',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->name, ['category/view','id'=>$data->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->user->username, ['user/view','id'=>$data->user->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            [
                'attribute' => 'parent_id',
                'label' => 'Parent',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    if ($data->parent) {
                        return
                            Html::a($data->parent->name, ['category/view','id'=>$data->parent->id], ['title' => 'View', 'class' => 'regular']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'access',
                'value' => function($model)
                {
                    if (isset($model->access)) {
                        return Category::ACCESS[$model->access];
                    }
                    return null;
                }
            ],
            'create_date',
            'update_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
