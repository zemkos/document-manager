<?php

use common\models\Category;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->user->username, ['user/view','id'=>$data->user->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            [
                'attribute' => 'parent_id',
                'label' => 'Parent',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    if ($data->parent) {
                        return
                            Html::a($data->parent->name, ['category/view','id'=>$data->parent->id], ['title' => 'View', 'class' => 'regular']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'access',
                'value' => function($model)
                {
                    if (isset($model->access)) {
                        return Category::ACCESS[$model->access];
                    }
                    return null;
                }
            ],
            'create_date',
            'update_date',
            [
                'attribute' => 'delete_date',
                'visible' => !empty($model->delete_date)
            ],
        ],
    ]) ?>

</div>
