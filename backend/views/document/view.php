<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Document */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="document-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'filename',
            'path',
            'version',
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->user->username, ['user/view','id'=>$data->user->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            [
                'attribute' => 'category_id',
                'label' => 'Category',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->category->name, ['category/view','id'=>$data->category->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            'create_date',
            'update_date',
            [
                'attribute' => 'delete_date',
                'visible' => !empty($model->delete_date)
            ],
        ],
    ]) ?>

</div>
