<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'title',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->title, ['document/view','id'=>$data->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            'filename',
            'version',
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->user->username, ['user/view','id'=>$data->user->id], ['title' => 'View', 'class' => 'regular']);
                }
            ],
            [
                'attribute' => 'category_id',
                'label' => 'Category',
                'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                'format' => 'raw',
                'value' => function($data) {
                    return
                        Html::a($data->category->name, ['category/view','id'=>$data->category->id], ['title' => 'View', 'class' => 'regular']);

                }
            ],
            'create_date',
            'update_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
