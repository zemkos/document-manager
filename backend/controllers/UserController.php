<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserSearch;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index','create','update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','create','update','view','delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $user = new User();

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->setPassword();

            try {
                $user->save();
                Yii::$app->session->setFlash('success', 'User created.');
                return $this->redirect(['view', 'id' => $user->id]);
            } catch (\Exception $e) {
                Yii::error($e->getMessage());
                Yii::$app->session->setFlash('danger', 'Failed to save.');
            }
        }

        return $this->render('create', [
            'model' => $user,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $oldpassword = $user->password;

        if ($user->load(Yii::$app->request->post())) {
            if (isset($user->password) && $user->password != $oldpassword) {
                $user->setPassword();
            }
            try {
                $user->save();
                Yii::$app->session->setFlash('success', 'User updated.');
                return $this->redirect(['view', 'id' => $user->id]);
            } catch (\Exception $e) {
                Yii::error($e->getMessage());
                Yii::$app->session->setFlash('danger', 'Failed to save.');
            }
        }

        return $this->render('update', [
            'model' => $user,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);

        try {
            $user->safeDelete();
            Yii::$app->session->setFlash('success', 'Successful delete.');
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('danger', 'Failed delete.');
            Yii::error("Delete failed.".VarDumper::dumpAsString($e));
        }

        return $this->redirect(['view', 'id' => $user->id]);

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
