<?php

namespace backend\controllers;

use common\models\Document;
use Yii;
use common\models\Category;
use backend\models\CategorySearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index','create','update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','create','update','view','delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $category = new Category();

        if ($category->load(Yii::$app->request->post())) {
            try {
                $category->save();
                Yii::$app->session->setFlash('success', 'Category updated.');

                return $this->redirect(['view', 'id' => $category->id]);
            } catch (\Exception $e) {
                Yii::error($e->getMessage());
                Yii::$app->session->setFlash('danger', 'Failed to save.');
            }
        }

        return $this->render('create', [
            'model' => $category,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $category = $this->findModel($id);

        if ($category->load(Yii::$app->request->post())) {
            try {
                $category->save();
                Yii::$app->session->setFlash('success', 'User updated.');

                return $this->redirect(['view', 'id' => $category->id]);
            } catch (\Exception $e) {
                Yii::error($e->getMessage());
                Yii::$app->session->setFlash('danger', 'Failed to save.');
            }
        }

        return $this->render('update', [
            'model' => $category,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $children =  $category->getCategories()->active()->all();
            if ($children) {
                foreach ($children as $child) {
                    $child->parent_id = $category->parent_id;
                    if (!$child->save()) {
                        throw new \Exception("failed to save category, id=" . $this->id);
                    }
                }
            }
            $documents = Document::find()->category($category->id)->active()->all();
            if ($documents) {
                foreach ($documents as $document) {
                    $document->safeDelete();
                }
            }
            $category->safeDelete();

            $transaction->commit();
            Yii::$app->session->setFlash('success', 'Successful delete.');
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', 'Failed delete.');
            Yii::error("Delete failed.".VarDumper::dumpAsString($e));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
