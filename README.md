# Document Maneger
#### Telepítés (Docker)

1. Framework és fűggőségeinek a telepítése:
```
 $ docker-compose run --rm backend composer install
```

2. Alkalmazás inicializálása:
```
 $ docker-compose run --rm backend /app/init
```

3. Docker containerek indítása:
```
 $ docker-compose up -d
```

4. Adatbázis kapcsolat beállítása (common/config/main-local.php):
```
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'pgsql:host=<container_name>;dbname=document',
        'username' => 'document',
        'password' => 'document',
        'charset' => 'utf8',
    ]
```
5. Migráció futtatása:
```
 $ docker-compose run --rm backend yii migrate
```

6. SQL dump betöltése:
```
 $ cat dump.sql | sudo docker exec -i <container_id> psql -U <username> -d <dbname>
```
 Ha sikerült betölteni a dumpot a következő admin fiókkal léphet be:
 ```
 username:  admin
 password:  admin
 ```
 
 A telepítést követően a localhost:8000 (frontend) és a localhost:8001 (admin) címeken érhető el az alkalmazás.
 
 *A frontend/web/uploads jogosulságaira figyelni kell, mert ha az alkalmazásnak nincs írási joga a folderban, a fájlfeltöltés sikertelen lesz.*
