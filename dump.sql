--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.21
-- Dumped by pg_dump version 9.5.21

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: category; Type: TABLE; Schema: public; Owner: document
--

CREATE TABLE public.category (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    user_id integer,
    parent_id integer,
    access integer,
    create_date timestamp(0) without time zone DEFAULT now(),
    update_date timestamp(0) without time zone DEFAULT now(),
    delete_date timestamp(0) without time zone
);


ALTER TABLE public.category OWNER TO document;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: document
--

CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO document;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: document
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: document; Type: TABLE; Schema: public; Owner: document
--

CREATE TABLE public.document (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    filename character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    version double precision NOT NULL,
    category_id integer,
    user_id integer,
    create_date timestamp(0) without time zone DEFAULT now(),
    update_date timestamp(0) without time zone DEFAULT now(),
    delete_date timestamp(0) without time zone
);


ALTER TABLE public.document OWNER TO document;

--
-- Name: document_id_seq; Type: SEQUENCE; Schema: public; Owner: document
--

CREATE SEQUENCE public.document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_id_seq OWNER TO document;

--
-- Name: document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: document
--

ALTER SEQUENCE public.document_id_seq OWNED BY public.document.id;


--
-- Name: migration; Type: TABLE; Schema: public; Owner: document
--

CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time integer
);


ALTER TABLE public.migration OWNER TO document;

--
-- Name: user; Type: TABLE; Schema: public; Owner: document
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(255) NOT NULL,
    admin boolean DEFAULT false,
    create_date timestamp(0) without time zone DEFAULT now(),
    update_date timestamp(0) without time zone DEFAULT now(),
    delete_date timestamp(0) without time zone
);


ALTER TABLE public."user" OWNER TO document;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: document
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO document;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: document
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.document ALTER COLUMN id SET DEFAULT nextval('public.document_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: document
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: document
--

COPY public.category (id, name, user_id, parent_id, access, create_date, update_date, delete_date) FROM stdin;
1	Root	1	\N	21	2020-02-22 16:54:00	2020-02-22 16:54:00	\N
2	Root 2	1	\N	21	2020-02-22 16:54:19	2020-02-22 16:54:19	\N
3	Root 3	1	\N	21	2020-02-22 16:54:44	2020-02-22 16:54:44	\N
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: document
--

SELECT pg_catalog.setval('public.category_id_seq', 3, true);


--
-- Data for Name: document; Type: TABLE DATA; Schema: public; Owner: document
--

COPY public.document (id, title, filename, path, version, category_id, user_id, create_date, update_date, delete_date) FROM stdin;
\.


--
-- Name: document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: document
--

SELECT pg_catalog.setval('public.document_id_seq', 1, false);


--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: document
--

COPY public.migration (version, apply_time) FROM stdin;
m000000_000000_base	1582202459
m200215_100231_init	1582390253
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: document
--

COPY public."user" (id, username, password, admin, create_date, update_date, delete_date) FROM stdin;
1	admin	$2y$13$trFmAjRxefjyqwyR6TyzaOU43Kwsffi1CApwvnfT7ahfSe5KQV/Q2	t	2020-02-22 16:52:25	2020-02-22 16:52:25	\N
\.


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: document
--

SELECT pg_catalog.setval('public.user_id_seq', 1, true);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: document_pkey; Type: CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id);


--
-- Name: migration_pkey; Type: CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_username_key; Type: CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: fk_category_category; Type: FK CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT fk_category_category FOREIGN KEY (parent_id) REFERENCES public.category(id);


--
-- Name: fk_category_user; Type: FK CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT fk_category_user FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: fk_document_category; Type: FK CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT fk_document_category FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: fk_document_user; Type: FK CONSTRAINT; Schema: public; Owner: document
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT fk_document_user FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: document
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM document;
GRANT ALL ON SCHEMA public TO document;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

