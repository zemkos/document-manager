<?php

use common\models\Category;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = 'Update Category: ' . $model->name;
?>
<div class="new-subcategory">

    <h2><?= Html::encode($this->title) ?></h2>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'access')->dropDownList(
        Category::ACCESS,
        ['prompt'=>'Choose permission level for other users']
    ); ?>

    <div class="d-flex">
        <div class="mr-auto p-2">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
        <div class="p-2">
            <?= Html::a('Delete Category', ['delete-category', 'id' => $model->id], [
                'class' => 'btn btn-outline-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete the "' . $model->name
                        . '" category, with all documents and subcategories?' ,
                    'method' => 'post',
                ],
            ]);?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
