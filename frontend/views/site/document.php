<?php

use common\models\Document;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $document common\models\Document */
/* @var $previousProvider yii\data\ActiveDataProvider */

$this->title = $document->title;
$this->params['breadcrumbs'][] = ['label' => $document->category->name, 'url' => ['index', 'id' => $document->category_id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="document-view">
    <h2><?= Html::encode($this->title) ?></h2>
    <div class="row">
        <div class="col-md-6 d-flex align-items-start flex-column">
            <div class="mb-auto p-2">
                <?= DetailView::widget([
                    'model' => $document,
                    'options' => ['class' => 'table'],
                    'attributes' => [
                        'filename',
                        'version',
                        [
                            'attribute' => 'user_id',
                            'label' => 'Author',
                            'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                            'format' => 'raw',
                            'value' => function(Document $data) {
                                return $data->user->username;
                            }
                        ],
                        [
                            'attribute' => 'category_id',
                            'label' => 'Category',
                            'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                            'format' => 'raw',
                            'value' => function(Document $data) {
                                return
                                    Html::a($data->category->name, ['category/view','id'=>$data->category->id], ['title' => 'View', 'class' => 'regular']);
                            }
                        ],
                        [
                            'attribute' => 'create_date',
                            'label' => 'Date',
                        ]
                    ],
                ]) ?>
            </div>
            <div class="p-2">
                <?= Html::a('Download', ['download', 'id' => $document->id], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <div class="col-md-6">
            <?php
            if ($previousProvider) {
            ?>
                <h4 class="p-1"><?= Html::encode('Other versions:') ?></h4>
                <?= GridView::widget([
                    'dataProvider' => $previousProvider,
                    'tableOptions' => ['class' => 'table table-hover w-100'],
                    'summary' => '',
                    'columns' => [
                        [
                            'attribute' => 'title',
                            'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                            'format' => 'raw',
                            'value' => function (Document $data) {
                                return Html::a($data->title, ['document', 'id' => $data->id],
                                    ['title' => 'View', 'class' => 'regular']);
                            }
                        ],
                        'version',
                        [
                            'attribute' => 'user_id',
                            'label' => 'Author',
                            'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                            'format' => 'raw',
                            'value' => function (Document $data) {
                                return $data->user->username;
                            }
                        ],
                    ],
                ]) ?>
            <?php } ?>
        </div>
    </div>



</div>

