<?php

use common\models\Category;
use common\models\Document;
use leandrogehlen\treegrid\TreeGrid;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $categoryProvider yii\data\ActiveDataProvider */
/* @var $documentProvider yii\data\ActiveDataProvider */
/* @var $category Category */

?>
<div class="row">
    <div class="col-xl-4">
        <?= TreeGrid::widget([
            'dataProvider' => $categoryProvider,
            'keyColumnName' => 'id',
            'parentColumnName' => 'parent_id',
            'parentRootValue' => null,
            'options' =>  ['class' => 'table'],
            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'Categories',
                    'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                    'format' => 'raw',
                    'value' => function(Category $data) {
                        if (!Yii::$app->user->isGuest) {
                            $user = Yii::$app->user->identity;
                            if ($data->accessToCategory(Category::ACCESS_READ)) {
                                return
                                    Html::a($data->name, ['index','id'=>$data->id],
                                        ['title' => 'View', 'class' => 'regular']);
                            }
                        }
                        return $data->name . '<i class="access-denied"> - Access denied.</i>';
                    }
                ],
            ]
        ]); ?>
    </div>

    <div class="col-xl-8">
        <?php
        if ($documentProvider) { ?>
            <h2><?= Html::encode($category->name) ?></h2>

            <div class="d-flex flex-row-reverse">
                <div class="p-1">
                    <?php
                    if ($category->accessToCategory(Category::ACCESS_ADMIN)) {
                        echo Html::button('Update Category', [
                            'value'=> Url::to('update-category?id=' . $_GET['id']),
                            'class' => 'btn btn-outline-dark',
                            'id' => 'updateCategoryButton'
                        ]);

                    }  ?>
                </div>
                <div class="p-1">
                    <?php
                    if($category->accessToCategory(Category::ACCESS_WRITE)) {
                        echo Html::button('New Subcategory', [
                            'value'=> Url::to('new-subcategory?id=' . $_GET['id']),
                            'class' => 'btn btn-outline-primary',
                            'id' => 'newSubcategoryButton'
                        ]);
                    } ?>
                </div>
            </div>
            <?php
            echo GridView::widget([
                'dataProvider' => $documentProvider,
                'tableOptions' => ['class' => 'table table-hover'],
                'options' => ['class' => 'mt-3 mb-3 w100'],
                'summary' => '',
                'columns' => [
                    [
                        'attribute' => 'title',
                        'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                        'format' => 'raw',
                        'value' => function (Document $data) {
                            return Html::a($data->title, ['document', 'id' => $data->id],
                                ['title' => 'View', 'class' => 'regular']);
                        }
                    ],
                    'filename',
                    'version',
                    [
                        'attribute' => 'user_id',
                        'label' => 'Author',
                        'contentOptions' => ['style' => 'font-size:16px; font-weight: bold'],
                        'format' => 'raw',
                        'value' => function (Document $data) {
                            return $data->user->username;
                        }
                    ],
                ],
            ]);

            if($category->accessToCategory(Category::ACCESS_WRITE)) {
                echo Html::button('New Document', [
                    'value'=> Url::to('new-document?id=' . $_GET['id']),
                    'class' => 'btn btn-primary',
                    'id' => 'newDocumentButton'
                ]);
            }
        }
        ?>

    </div>
</div>


