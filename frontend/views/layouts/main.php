<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use frontend\assets\AppAsset;
use yii\bootstrap4\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
Modal::begin([
    'id' => 'modal',
    'size'=>'modal-md',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>


<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-expand-lg navbar-dark bg-dark',
    ],
]);
$menuItems = [
    ['label' => 'Home', 'url' => ['/site/index']],
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = ['label' => 'Logout (' . Yii::$app->user->identity->username . ')', 'url' => ['/site/logout']];
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ml-auto'],
    'items' => $menuItems,
]);
NavBar::end();
?>

<div class="container wrap">
    <?= Breadcrumbs::widget([
        'links' => $this->params['breadcrumbs'] ?? [],
        'homeLink' => [
            'label' => 'Home',
            'url' => Yii::$app->homeUrl,
        ],
        'options' => [
            'class' => 'mt-3 mb-3'
        ]
    ]) ?>

    <?php if (!empty(Yii::$app->session->getAllFlashes())) {
        foreach (Yii::$app->session->getAllFlashes() as $type => $flash) { ?>
            <?= Alert::widget([
                'options' => [
                    'class' => "alert-{$type} m-3",
                ],
                'body' => $flash
            ]) ?>
        <?php } ?>
    <?php } ?>

    <div class="mt-3 mb-3">
        <?= $content ?>
    </div>
</div>


<footer class="bg-light p-3 footer">
    <div class="container">
        &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
