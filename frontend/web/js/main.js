$(function () {
    "use strict";
    $('#newDocumentButton, #newSubcategoryButton, #updateCategoryButton').click(function () {
        $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
    });
});