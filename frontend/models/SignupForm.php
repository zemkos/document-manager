<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $password;
    public $password_repeat;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message' => 'Username cannot be blank.'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'The username is already being used.'],
            ['username', 'string', 'min' => 3, 'max' => 50],

            ['password', 'required', 'message' => 'Password cannot be blank.'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Signs user up.
     *
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user = $this->fillTo($user);
        $user->setPassword();
        $user->save();

        return $user;
    }

    public function fillFrom(User $user): SignupForm
    {
        $this->username = $user->username;
        $this->password = $user->password;

        return $this;
    }

    public function fillTo(User $user): User
    {
        $user->username = $this->username;
        $user->password = $this->password;

        return $user;
    }
}
