<?php
namespace frontend\models;

use common\models\Category;
use common\models\Document;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class DocumentForm extends Model
{

    public $title;
    public $file;
    public $version;
    public $user_id;
    public $category_id;
    public $path;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'file', 'version'], 'required'],
            [['version'], 'number'],
            [['category_id', 'user_id'], 'integer'],
            ['title', 'string', 'max' => 50],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return Document
     */
    public function fillTo()
    {
        $document = new Document();

        $document->title = $this->title;
        $document->user_id = $this->user_id;
        $document->category_id = $this->category_id;
        $document->filename = $this->file;
        $document->version = $this->version;
        $document->path = $this->path;

        return $document;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function upload()
    {
        $file = UploadedFile::getInstance($this,'file');

        if (!isset($file)) {
            throw new \Exception('Failed to upload file.');
        }
        $filename = $file->getBaseName() . '.' . $file->getExtension();
        $previous = Document::find()->filename($filename)->category($this->category_id)->latestVersion()->active()->one();

        if ($previous && $this->version <= $previous->version) {
            $this->version = $previous->version + 1;
        }
        $this->path =  $file->getBaseName() . '_v' . $this->version .  '.' . $file->getExtension();
        $file->saveAs(Yii::getAlias('@filePath').'/'. $this->path);
        $this->file = $filename;

        return $this;
    }
}