<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\Document;
use common\models\DocumentQuery;
use frontend\models\DocumentForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\SignupForm;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'login', 'logout', 'signup', 'document', 'new-document',
                    'update-category', 'delete-category', 'new-subcategory', 'download'
                ],
                'rules' => [
                    [
                        'actions' => ['login', 'signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'logout', 'document', 'new-document', 'update-category',
                            'delete-category', 'new-subcategory', 'download'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionIndex($id = null)
    {
        $categoryProvider = new ActiveDataProvider([
            'query' => Category::find()->active(),
            'pagination' => false
        ]);

        $documentProvider = $title = null;
        $category = $id ? Category::find()->ofID($id)->active()->one() : null;

        if ($category && $category->accessToCategory(Category::ACCESS_READ)) {
            $documentProvider = new ActiveDataProvider([
                'query' => DocumentQuery::getLatest($id),
            ]);

        }

        return $this->render('index', [
            'categoryProvider' => $categoryProvider,
            'documentProvider' => $documentProvider,
            'category' => $category,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionDocument($id)
    {
        $document = Document::find()->ofID($id)->active()->one();
        $previousProvider = null;
        if ($document && $document->category->accessToCategory(Category::ACCESS_READ)) {
            $previousQuery = Document::find()->category($document->category_id)->active()
                ->filename($document->filename)->notEqualWithID($id)->latestVersion();

            if ($previousQuery->all()) {
                $previousProvider = new ActiveDataProvider([
                    'query' => $previousQuery,
                ]);
            }

            return $this->render('document', [
                'document' => $document,
                'previousProvider' => $previousProvider,
            ]);
        }
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionNewDocument($id)
    {
        $form = new DocumentForm();
        $category = Category::find()->ofID($id)->active()->one();

        if ($category && $category->accessToCategory(Category::ACCESS_WRITE) && $form->load($_POST)) {
            try {
                $form->user_id = Yii::$app->user->id;
                $form->category_id = $id;
                $form->upload();
                $form->fillTo()->save();
                Yii::$app->session->setFlash('success', 'New document successfully added.');
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('danger', 'Failed to upload document');
                Yii::error("Failed to save model.".VarDumper::dumpAsString($e));
            }

            return $this->redirect(['index', 'id' => $id]);
        }

        return $this->renderAjax('new-document', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdateCategory($id)
    {
        $category = Category::find()->ofID($id)->active()->one();

        if ($category->load($_POST) && $category->accessToCategory(Category::ACCESS_ADMIN)) {
            $category->update_date = date('Y-m-d H:i:s');
            try {
                $category->save();
                Yii::$app->session->setFlash('success', '"' . $category->name . '" category successfully updated.');
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('danger', 'Failed to update category.');
                Yii::error("Failed to save model.".VarDumper::dumpAsString($e));
            }

            return $this->redirect(['index', 'id' => $id]);
        }

        return $this->renderAjax('update-category', [
            'model' => $category,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeleteCategory($id)
    {
        $category = Category::find()->ofID($id)->active()->one();
        $subcategories = $category->getSubcategories();
        if ($category && $category->accessToCategory(Category::ACCESS_ADMIN)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($subcategories) {
                    foreach ($subcategories as $subcategory) {
                        $documents =  $subcategory->documents;
                        if ($documents) {
                            foreach ($documents as $document) {
                                $document->safeDelete();
                            }
                        }
                        $subcategory->safeDelete();
                    }
                }

                $documents = $category->documents;
                if ($documents) {
                    foreach ($documents as $document) {
                        $document->safeDelete();
                    }
                }
                $category->safeDelete();

                Yii::$app->session->setFlash('info', 'Category deleted.');
                $transaction->commit();

            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('danger', 'Failed to delete category.');
                Yii::error("Delete failed.".VarDumper::dumpAsString($e));
            }
        }

        return $this->redirect(['index', 'id' => $category->parent_id]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionNewSubcategory($id)
    {
        $category = new Category();
        $parent = Category::find()->ofID($id)->active()->one();

        if ($category->load(Yii::$app->request->post()) && $parent->accessToCategory(Category::ACCESS_WRITE)) {
            $category->user_id = Yii::$app->user->id;
            $category->parent_id = $id;
            try {
                $category->save();
                Yii::$app->session->setFlash('success', '"' . $category->name . '" subcategory successfully created.');
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('danger', 'Failed to create subcategory.');
                Yii::error("".VarDumper::dumpAsString($e));
            }

            return $this->redirect(['index', 'id' => $id]);
        }

        return $this->renderAjax('new-subcategory', [
            'model' => $category,
        ]);
    }

    /**
     * @param $id
     * @return \yii\console\Response|\yii\web\Response
     */
    public function actionDownload($id)
    {
        $document = Document::find()->ofID($id)->active()->one();

        if ($document && $document->category->accessToCategory(Category::ACCESS_READ)) {
            $path = Yii::getAlias('@filePath') . '/';
            $file = $path . $document->path;

            if (file_exists($file)) {
               return Yii::$app->response->sendFile($file);
            }
        }

        Yii::$app->session->setFlash('danger', 'Download failed: the file does not exist or permission denied.');
        return $this->redirect(['document', 'id' => $id]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $signupForm = new SignupForm();
        if ($signupForm->load(Yii::$app->request->post())) {
            try {
                $user = $signupForm->signup();
                $loginForm = new LoginForm();
                $loginForm->fillFrom($signupForm)->login();
                Yii::$app->session->setFlash('success', 'Thank you for registration.');

                return $this->goHome();
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('danger', 'Failed to create subcategory.');
                Yii::error("".VarDumper::dumpAsString($e));
            }
        }

        return $this->render('signup', [
            'model' => $signupForm,
        ]);
    }
}
