<?php

namespace common\models;

use kartik\tree\models\Tree;
use phpDocumentor\Reflection\Types\This;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property int|null $user_id
 * @property int|null $parent_id
 * @property int|null $access
 * @property string|null $create_date
 * @property string|null $update_date
 * @property string|null $delete_date
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property User $user
 * @property Document[] $documents
 */
class Category extends ActiveRecord
{
    const ACCESS_PRIVATE = 0;
    const ACCESS_READ = 12;
    const ACCESS_WRITE = 21;
    const ACCESS_ADMIN = 22;

    const ACCESS = [
        0 => 'PRIVATE',
        12 => 'READ-ONLY',
        21 => 'PUBLIC',
        22 => 'ADMIN',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'access'], 'required'],
            [['user_id', 'parent_id'], 'default', 'value' => null],
            [['user_id', 'parent_id'], 'integer'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_id' => 'User ID',
            'parent_id' => 'Parent ID',
            'access' => 'Access',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'delete_date' => 'Delete Date',
        ];
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::class, ['id' => 'parent_id'])->active();
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['parent_id' => 'id'])->active();
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->active();
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery|DocumentQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::class, ['category_id' => 'id'])->active();
    }

    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @throws \yii\db\Exception
     */
    public function safeDelete()
    {
        $this->delete_date = date('Y-m-d H:i:s');
        $this->save();;
    }

    /**
     * @param $level
     * @return bool
     */
    public function accessToCategory($level): bool
    {
        $user = Yii::$app->user->identity;
        return ($user && ($user->isAdmin() || $this->user_id == $user->id
                || in_array($level, self::getAccess($this->access))));
    }

    public static function getAccess(int $access): array
    {
        switch ($access) {
            case self::ACCESS_ADMIN:
                return [self::ACCESS_ADMIN, self::ACCESS_WRITE, self::ACCESS_READ, self::ACCESS_PRIVATE];
                break;
            case self::ACCESS_WRITE:
                return  [self::ACCESS_WRITE, self::ACCESS_READ, self::ACCESS_PRIVATE];
                break;
            case self::ACCESS_READ:
                return  [self::ACCESS_READ, self::ACCESS_PRIVATE];
                break;
            case self::ACCESS_PRIVATE:
                return  [self::ACCESS_PRIVATE];
                break;
            default:
                return [];
                break;
        }
    }

    /**
     * @return Category[]|[]
     */
    public function getSubcategories(): array
    {
        $categories = $this->categories;
        $subcategories = array();

        foreach ($categories as $category) {
            $subsubs = $category->getSubcategories();
            if ($subsubs) {
                foreach ($subsubs as $subsub) {
                    $subcategories[] = $subsub;
                }
            }
            $subcategories[] = $category;
        }

        return $subcategories;
    }
}
