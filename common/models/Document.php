<?php

namespace common\models;

use frontend\models\DocumentForm;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string $title
 * @property string $filename
 * @property string $path
 * @property float $version
 * @property int|null $category_id
 * @property int|null $user_id
 * @property string|null $create_date
 * @property string|null $update_date
 * @property string|null $delete_date
 *
 * @property Category $category
 * @property User $user
 */
class Document extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'filename', 'path', 'version'], 'required'],
            [['version'], 'number'],
            [['category_id', 'user_id'], 'default', 'value' => null],
            [['category_id', 'user_id'], 'integer'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['title', 'filename'], 'string', 'max' => 255],
            [['path'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'filename' => 'Filename',
            'path' => 'Path',
            'version' => 'Version',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'delete_date' => 'Delete Date',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return DocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentQuery(get_called_class());
    }

    /**
     * @throws \yii\db\Exception
     */
    public function safeDelete()
    {
        $this->delete_date = date('Y-m-d H:i:s');
        $this->save();
    }
}
