<?php

namespace common\models;

use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[Document]].
 *
 * @see Document
 */
class DocumentQuery extends \yii\db\ActiveQuery
{

    /**
     * {@inheritdoc}
     * @return Document[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Document|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active(): DocumentQuery
    {
        return $this->andWhere(['delete_date' => null]);
    }

    public function ofID($id): DocumentQuery
    {
        return $this->andWhere(['id' => $id]);
    }

    public function category($id): DocumentQuery
    {
        return $this->andWhere(['category_id' => $id]);
    }

    public function filename($file): DocumentQuery
    {
        return $this->andWhere(['filename' => $file]);
    }

    public function latestVersion(): DocumentQuery
    {
        return $this->orderBy(['version' => SORT_DESC]);
    }

    public function notEqualWithID($id): DocumentQuery
    {
        return $this->andWhere(['!=', 'id', $id]);
    }

    public static function getLatest($categoryId): DocumentQuery
    {
        $sql = "SELECT * FROM document AS d1 
                    WHERE version = (
                        SELECT max(version) FROM document as d2 
                        WHERE d1.filename = d2.filename 
                        AND d1.category_id = d2.category_id
                    ) AND category_id = " . $categoryId;

        /** @var  DocumentQuery $query */
        $query = Document::findBySql($sql);

        return $query;
    }
}
