<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends \yii\db\ActiveQuery
{

    /**
     * {@inheritdoc}
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ofUsername($username): UserQuery
    {
        return $this->andWhere(['username' => $username]);
    }

    public function active(): UserQuery
    {
        return $this->andWhere(['delete_date' => null]);
    }

    public function admin(): UserQuery
    {
        return $this->andWhere(['admin' => true]);
    }
}
