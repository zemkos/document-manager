<?php

namespace common\models;

use yii\db\Exception;
use yii\helpers\VarDumper;

class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool|void
     * @throws Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!parent::save($runValidation, $attributeNames)) {
            throw new Exception("failed to save model: " . VarDumper::dumpAsString($this->getErrors()));
        }
    }

}