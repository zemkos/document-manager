<?php
Yii::setAlias('@filePath', '/app/frontend/web/uploads');
Yii::setAlias('@fileUrl', 'web/uploads/');

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
