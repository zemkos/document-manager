<?php

use yii\db\Migration;

/**
 * Class m200215_100231_init
 */
class m200215_100231_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50)->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'admin' => $this->boolean()->defaultValue(false),
            'create_date' => $this->timestamp()->defaultExpression('now()'),
            'update_date' => $this->timestamp()->defaultExpression('now()'),
            'delete_date' => $this->timestamp(),
        ]);

        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'user_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'access' => $this->integer(),
            'create_date' => $this->timestamp()->defaultExpression('now()'),
            'update_date' => $this->timestamp()->defaultExpression('now()'),
            'delete_date' => $this->timestamp(),
        ]);

        $this->addForeignKey('fk_category_user','category','user_id','user',
            'id');

        $this->addForeignKey('fk_category_category','category','parent_id','category',
            'id');

        $this->createTable('document', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'filename' => $this->string()->notNull(),
            'path' => $this->string()->notNull(),
            'version' => $this->double()->notNull(),
            'category_id' => $this->integer(),
            'user_id' => $this->integer(),
            'create_date' => $this->timestamp()->defaultExpression('now()'),
            'update_date' => $this->timestamp()->defaultExpression('now()'),
            'delete_date' => $this->timestamp(),
        ]);

        $this->addForeignKey('fk_document_category','document','category_id','category',
            'id');

        $this->addForeignKey('fk_document_user','document','user_id','user',
            'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_document_user','document');
        $this->dropForeignKey('fk_document_category','document');
        $this->dropTable('document');
        $this->dropForeignKey('fk_category_category','category');
        $this->dropForeignKey('fk_category_user','category');
        $this->dropTable('category');
        $this->dropTable('user');
    }

}
